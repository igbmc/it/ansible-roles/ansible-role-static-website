Ansible Role Static Website
===========================

Role to configure a server to host a static website served by NGiNX
Access to the server is done by sFTP/SSH

Role Variables
--------------

webmasters : list of users (already available on the server) that are allowed to modifiy the website content

Example Playbook
----------------

    - hosts: servers
      roles:
         - { role: igbmc.static-website, webmasters: john,alice }

License
-------

GPL